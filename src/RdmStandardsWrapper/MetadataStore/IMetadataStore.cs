﻿using System;
using System.Collections.Generic;
using VDS.RDF;

namespace MetadataTracker.MetadataStore
{
    public interface IMetadataStore
    {
        void AddGraph(IGraph graph);
        void AddTripleStore(ITripleStore tripleStore);
        bool HasGraph(Uri uri);
        bool HasGraph(string absoluteUri);
        IGraph GetGraph(Uri uri);
        IGraph GetGraphOrEmpty(Uri uri);
        IEnumerable<Uri> ListGraphs(string id);
        void UpdateGraph(IGraph graph);
    }
}
