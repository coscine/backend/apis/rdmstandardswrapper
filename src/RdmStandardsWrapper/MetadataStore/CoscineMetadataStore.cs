﻿using Coscine.Metadata;
using VDS.RDF;
using VDS.RDF.Query;

namespace MetadataTracker.MetadataStore
{
    public class CoscineMetadataStore : IMetadataStore
    {
        private readonly RdfStoreConnector _rdfStoreConnector;

        public CoscineMetadataStore()
        {
            _rdfStoreConnector = new RdfStoreConnector();
        }

        public CoscineMetadataStore(Coscine.Configuration.IConfiguration configuration)
        {
            _rdfStoreConnector = new RdfStoreConnector(configuration.GetStringAndWait("coscine/local/virtuoso/additional/url"));
        }

        public void AddGraph(IGraph graph)
        {
            _rdfStoreConnector.AddGraph(graph);
        }

        public void AddTripleStore(ITripleStore tripleStore)
        {
            foreach (var entryGraph in tripleStore.Graphs)
            {
                AddGraph(entryGraph);
            }
        }

        public IGraph GetGraph(Uri uri)
        {
            return _rdfStoreConnector.GetGraph(uri);
        }

        public IGraph GetGraphOrEmpty(Uri uri)
        {
            if (uri != null && HasGraph(uri))
            {
                return GetGraph(uri);
            }
            return new Graph()
            {
                BaseUri = uri
            };
        }

        public bool HasGraph(Uri uri)
        {
            return HasGraph(uri.AbsoluteUri);
        }

        public bool HasGraph(string absoluteUri)
        {
            return _rdfStoreConnector.HasGraph(absoluteUri);
        }

        public IEnumerable<Uri> ListGraphs(string id)
        {
            var cmdString = new SparqlParameterizedString
            {
                CommandText = @"SELECT DISTINCT ?g
                WHERE { GRAPH ?g { ?s ?p ?o }
                FILTER(contains(str(?g), @graph)) }"
            };
            cmdString.SetLiteral("graph", id);

            var resultSet = _rdfStoreConnector.QueryEndpoint.QueryWithResultSet(cmdString.ToString());

            var graphs = new List<Uri>();
            foreach (SparqlResult r in resultSet)
            {
                graphs.Add((r.Value("g") as UriNode)?.Uri);
            }
            return graphs;
        }

        public void UpdateGraph(IGraph graph)
        {
            _rdfStoreConnector.ClearGraph(graph.BaseUri);
            AddGraph(graph);
        }
    }
}
