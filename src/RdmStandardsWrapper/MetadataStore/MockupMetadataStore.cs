﻿using System;
using System.Collections.Generic;
using VDS.RDF;

namespace MetadataTracker.MetadataStore
{
    public class MockupMetadataStore : IMetadataStore
    {
        public void AddGraph(IGraph graph)
        {
            return;
        }

        public void AddTripleStore(ITripleStore tripleStore)
        {
            return;
        }

        public IGraph GetGraph(Uri uri)
        {
            return null;
        }

        public IGraph GetGraphOrEmpty(Uri uri)
        {
            return new Graph()
            {
                BaseUri = uri
            };
        }

        public bool HasGraph(Uri uri)
        {
            return false;
        }

        public bool HasGraph(string absoluteUri)
        {
            return false;
        }

        public IEnumerable<Uri> ListGraphs(string id)
        {
            return new List<Uri>();
        }

        public void UpdateGraph(IGraph graph)
        {
            return;
        }
    }
}
