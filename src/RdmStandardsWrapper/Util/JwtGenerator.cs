﻿using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Cryptography;

namespace RdmStandardsWrapper.Util
{
    public class JwtGenerator
    {
        private readonly JwtSecurityTokenHandler _jwtSecurityTokenHandler;

        private readonly DateTime _centuryBegin;

        public Coscine.Configuration.IConfiguration Configuration { get; set; }

        public JwtGenerator(Coscine.Configuration.IConfiguration configuration)
        {
            Configuration = configuration;
            _jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
            _centuryBegin = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        }

        public string GenerateJwtToken(JwtPayload payload, string issuer, string subject, DateTime issuedAt, DateTime expires)
        {
            if (payload == null)
            {
                throw new ArgumentNullException("JwtPayload value is not set!");
            }

            using RSA rsa = RSA.Create();
            rsa.ImportFromPem(Configuration.GetStringAndWait("coscine/local/trellis/privatekey"));

            var signingCredentials = new SigningCredentials(new RsaSecurityKey(rsa), SecurityAlgorithms.RsaSha256);
            double totalSeconds = (expires - _centuryBegin).TotalSeconds;
            double totalSeconds2 = (issuedAt - _centuryBegin).TotalSeconds;
            payload.Add("iss", issuer);
            payload.Add("sub", subject);
            payload.Add("iat", (long)totalSeconds2);
            payload.Add("exp", (long)totalSeconds);
            JwtSecurityToken token = new JwtSecurityToken(new JwtHeader(signingCredentials), payload);
            return _jwtSecurityTokenHandler.WriteToken(token);
        }

    }
}
