﻿namespace RdmStandardsWrapper.Util
{
    public class VersionUtil
    {
        public static Uri? GetClosestVersion(IEnumerable<Uri> graphs, long givenVersion, string? filter = null, bool extracted = false)
        {
            var currentBestGraph = graphs.FirstOrDefault();
            var currentClosest = long.MaxValue;
            foreach (var graph in graphs)
            {
                var queryDictionary = System.Web.HttpUtility.ParseQueryString(
                    new Uri(graph.ToString().Replace("@", "?")).Query);
                var version = queryDictionary["version"];
                if (version == null || !long.TryParse(version, out long longVersion))
                {
                    continue;
                }
                var versionCloseness = Math.Abs(givenVersion - longVersion);
                if (currentClosest > versionCloseness && ((queryDictionary["extracted"] == null) == !extracted) && (filter == null || queryDictionary[filter] != null))
                {
                    currentClosest = versionCloseness;
                    currentBestGraph = graph;
                }
            }
            return currentBestGraph;
        }

        public static Uri? GetRecentVersion(IEnumerable<Uri> graphs, string? filter = null, bool extracted = false)
        {
            var currentBestGraph = graphs.FirstOrDefault();
            var currentBestVersion = 0L;
            foreach (var graph in graphs)
            {
                var queryDictionary = System.Web.HttpUtility.ParseQueryString(
                    new Uri(graph.ToString().Replace("@", "?")).Query);
                var version = queryDictionary["version"];
                if (version == null || !long.TryParse(version, out long longVersion))
                {
                    continue;
                }
                if (longVersion > currentBestVersion && ((queryDictionary["extracted"] == null) == !extracted) && (filter == null || queryDictionary[filter] != null))
                {
                    currentBestVersion = longVersion;
                    currentBestGraph = graph;
                }
            }
            return currentBestGraph;
        }
    }
}
