using Coscine.ApiCommons;
using Coscine.Configuration;
using Coscine.JwtHandler;
using MetadataTracker.MetadataStore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using RdmStandardsWrapper.Util;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace RdmStandardsWrapper.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ExtendedLDPController : ControllerBase
    {
        private static readonly HttpClientHandler clientHandler = new HttpClientHandler()
        {
            ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; }
        };
        private static readonly HttpClient HttpClient = new(clientHandler);

        private readonly ILogger<ExtendedLDPController> _logger;
        private readonly static Coscine.Configuration.IConfiguration _configuration = new ConsulConfiguration();
        private readonly IMetadataStore _metaStore = new CoscineMetadataStore(_configuration);
        private readonly Uri _trellis = new(_configuration.GetString("coscine/local/trellis/url", "http://localhost:8253"));
        private readonly JwtGenerator _jwtGenerator;
        private readonly JWTHandler _jwtHandler;

        public ExtendedLDPController(ILogger<ExtendedLDPController> logger)
        {
            _logger = logger;
            _jwtGenerator = new JwtGenerator(_configuration);
            _jwtHandler = new JWTHandler(_configuration);
        }

        [HttpGet("{*path}")]
        public async Task Get(string? path)
        {
            await ForwardRequestAsync(HttpMethod.Get, path);
        }

        [HttpPut("{*path}")]
        public async Task Put(string? path)
        {
            await Task.Run(() => Response.StatusCode = 403);
        }

        [HttpPost("{*path}")]
        public async Task Post(string? path)
        {
            await Task.Run(() => Response.StatusCode = 403);
        }

        [HttpPatch("{*path}")]
        public async Task Patch(string? path)
        {
            await Task.Run(() => Response.StatusCode = 403);
        }

        [HttpDelete("{*path}")]
        public async Task Delete(string? path)
        {
            await Task.Run(() => Response.StatusCode = 403);
        }

        [HttpOptions("{*path}")]
        public async Task Options(string? path)
        {
            await ForwardRequestAsync(HttpMethod.Options, path);
        }

        private async Task ForwardRequestAsync(HttpMethod method, string? path)
        {
            path ??= "";

            var pathSplit = path.Split('/');
            var identiferPart = "";
            if (pathSplit.Length > 1)
            {
                identiferPart = string.Join('/', pathSplit[1..]);
            }
            var workingOnEntity = pathSplit.Length > 1 && (identiferPart.IndexOf("/") != identiferPart.LastIndexOf("/") || (identiferPart.Contains("/") && !identiferPart.EndsWith("/")));

            var headers = Request.Headers;

            var noFilter = headers.ContainsKey("No-Filter");

            var additionalParameter = "";

            // Deal with missing type
            if (path.Contains("@type="))
            {
                // Type already defined
            }
            else if (workingOnEntity && !noFilter)
            {
                if (!path.EndsWith("/"))
                {
                    path += "/";
                }
                // Default to Metadata
                additionalParameter += "@type=metadata";
            }

            // Deal with versions
            if (path.Contains("&version"))
            {
                // Version already set, nothing to do here
            }
            else if (headers.ContainsKey("Accept-Datetime"))
            {
                var givenVersion = long.Parse(Convert.ToString((int)DateTime.Parse(headers["Accept-Datetime"]).Subtract(new DateTime(1970, 1, 1)).TotalSeconds));

                var graphs = _metaStore.ListGraphs($"https://purl.org/coscine/resources/{path}");
                var closestVersionGraph = VersionUtil.GetClosestVersion(graphs, givenVersion,
                    path.Contains("type=metadata") ? "metadata"
                        : (path.Contains("type=data") ? "data"
                            : null),
                    path.Contains("&extracted")
                );
                if (closestVersionGraph != null)
                {
                    var queryDictionary = System.Web.HttpUtility.ParseQueryString(
                        new Uri(closestVersionGraph.ToString().Replace("@", "?")).Query);
                    var version = queryDictionary["version"];
                    additionalParameter += "&version=" + version;
                }
            }
            else if (method == HttpMethod.Post)
            {
                if (headers.ContainsKey("slug") && !noFilter)
                {
                    if (headers["slug"].ToString().Contains("@type"))
                    {
                        headers["slug"] = headers["slug"].ToString() + "&version=" + long.Parse(Convert.ToString((int)DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalSeconds));
                    }
                }
            }
            else if (
                (method == HttpMethod.Get || method == HttpMethod.Patch || method == HttpMethod.Put || method == HttpMethod.Options || method == HttpMethod.Delete)
                && workingOnEntity && !noFilter
            )
            {
                var graphs = _metaStore.ListGraphs($"https://purl.org/coscine/{path}");
                var recentVersionGraph = VersionUtil.GetRecentVersion(graphs,
                    path.Contains("type=metadata") ? "metadata"
                        : (path.Contains("type=data") ? "data"
                            : null),
                    path.Contains("&extracted")
                );
                if (recentVersionGraph != null)
                {
                    var queryDictionary = System.Web.HttpUtility.ParseQueryString(
                        new Uri(recentVersionGraph.ToString().Replace("@", "?")).Query);
                    var version = queryDictionary["version"];
                    if (version != null)
                    {
                        additionalParameter += "&version=" + version;
                    }
                }
            }

            var message = CreateRequestMessage(method, path, headers, additionalParameter);

            var response = await HttpClient.SendAsync(message, HttpCompletionOption.ResponseHeadersRead, HttpContext.RequestAborted);
            // If 401 is returned, you sometimes need to try again and then it works
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                message = CreateRequestMessage(method, path, headers, additionalParameter);
                response = await HttpClient.SendAsync(message, HttpCompletionOption.ResponseHeadersRead, HttpContext.RequestAborted);
            }
            Response.StatusCode = (int)response.StatusCode;

            foreach (var header in response.Headers)
            {
                Response.Headers[header.Key] = header.Value.ToArray();
            }

            foreach (var header in response.Content.Headers)
            {
                Response.Headers[header.Key] = header.Value.ToArray();
            }
            // SendAsync removes chunking from the response. This removes the header so it doesn't expect a chunked response.
            Response.Headers.Remove("transfer-encoding");

            using (var responseStream = await response.Content.ReadAsStreamAsync())
            {
                await responseStream.CopyToAsync(Response.Body, 81920, HttpContext.RequestAborted);
            }
        }

        private HttpRequestMessage CreateRequestMessage(HttpMethod method, string? path, IHeaderDictionary headers, string additionalParameter)
        {
            var message = new HttpRequestMessage()
            {
                Method = method,
                RequestUri = new Uri(_trellis.ToString() + path + Request.QueryString.ToString() + additionalParameter)
            };

            if (Request.ContentLength > 0 || Request.Headers.ContainsKey("Transfer-Encoding"))
            {
                var streamContent = new StreamContent(Request.Body);
                message.Content = streamContent;
            }

            foreach (var header in headers)
            {
                try
                {
                    var headerName = header.Key;
                    var value = header.Value;

                    if (headerName.ToLower() == "Authorization".ToLower())
                    {
                        // Parse JWT Token and try to see if its a Coscine one
                        var securityKey = _jwtHandler.GetSecurityKey();
                        var tokenValidationParameters = new TokenValidationParameters
                        {
                            ValidAudience = "https://coscine.rwth-aachen.de",
                            ValidIssuer = "https://coscine.rwth-aachen.de",
                            ValidateIssuerSigningKey = true,
                            IssuerSigningKey = securityKey,
                            ValidateIssuer = false,
                            ValidateAudience = true
                        };
                        var token = value.ToString().Replace("Bearer ", "");

                        try
                        {
                            // If valid, create a one with WebId
                            new JwtSecurityTokenHandler().ValidateToken(token, tokenValidationParameters, out _);
                            var userId = Authenticator.GetUserId(_jwtHandler.GetContents(token));
                            message.Headers.Add(header.Key,
                                "Bearer " + _jwtGenerator.GenerateJwtToken(
                                    new JwtPayload(
                                        new List<Claim>()
                                        {
                                            new Claim("webid", $"https://purl.org/coscine/users/{userId}")
                                        }
                                    ),
                                    "https://purl.org/coscine/users/",
                                    userId,
                                    DateTime.UtcNow.AddMinutes(-1440),
                                    DateTime.UtcNow.AddMinutes(1440)
                                )
                            );
                            continue;
                        }
                        catch (Exception)
                        {
                            // If not valid, just forward it
                        }
                    }

                    if (header.Key.ToLower() != "Accept-Datetime".ToLower())
                    {
                        // From https://github.com/ProxyKit/ProxyKit
                        if (value.Count == 1)
                        {
                            string headerValue = value;
                            if (!message.Headers.TryAddWithoutValidation(headerName, headerValue))
                            {
                                message.Content?.Headers.TryAddWithoutValidation(headerName, headerValue);
                            }
                        }
                        else
                        {
                            string[] headerValues = value;
                            if (!message.Headers.TryAddWithoutValidation(headerName, headerValues))
                            {
                                message.Content?.Headers.TryAddWithoutValidation(headerName, headerValues);
                            }
                        }
                    }
                }
                catch
                {
                    // Filter weird headers
                }
            }

            message.Headers.Host = message.RequestUri.Authority;
            return message;
        }
    }
}
