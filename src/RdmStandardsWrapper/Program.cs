using NSwag;
using NSwag.Generation.Processors.Security;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddOpenApiDocument((settings) =>
{
    settings.Title = "RDM Standards Wrapper";
    settings.Version = "1.0.0";
    settings.OperationProcessors.Add(new OperationSecurityScopeProcessor("JWT token"));
    settings.DocumentProcessors.Add(new SecurityDefinitionAppender("JWT token", new OpenApiSecurityScheme
    {
        Type = OpenApiSecuritySchemeType.ApiKey,
        Name = "Authorization",
        Description = "Copy 'Bearer ' + valid JWT token into field",
        In = OpenApiSecurityApiKeyLocation.Header
    }));
});

var app = builder.Build();

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
    app.UseOpenApi(config =>
    {
        if (!app.Environment.IsDevelopment())
        {
            config.PostProcess = (document, _) => document.Schemes = new[] { OpenApiSchema.Https };
        }
    }); // serve OpenAPI/Swagger documents
    app.UseSwaggerUi3(); // serve Swagger UI
    app.UseReDoc(); // serve ReDoc UI
//}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
